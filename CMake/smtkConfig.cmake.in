#------------------------------------------------------------------------------
# Update CMAKE_PREFIX_PATH locally when SMTK_RELOCATABLE_INSTALL=OFF
#------------------------------------------------------------------------------

set(SMTK_PREFIX_PATH "@smtk_prefix_path@")
set(CMAKE_PREFIX_PATH_save "${CMAKE_PREFIX_PATH}")
list(APPEND CMAKE_PREFIX_PATH ${SMTK_PREFIX_PATH})

#Set up the CMake module paths
list(APPEND CMAKE_MODULE_PATH "@SMTK_MODULE_DIR@;@SMTK_VTK_MODULE_DIR@")
set(smtk_module_dir "@SMTK_VTK_MODULE_DIR@/Modules")
if(IS_DIRECTORY "${smtk_module_dir}")
  list(APPEND CMAKE_MODULE_PATH "${smtk_module_dir}")
  file(GLOB config_files RELATIVE "${smtk_module_dir}" "${smtk_module_dir}/*.cmake")
  foreach (_file IN LISTS config_files)
    if (NOT _file MATCHES "[^\\-]+-[a-zA-Z]+\\.cmake")
      string(REGEX REPLACE "\\.cmake$" "" _module "${_file}")
      list(APPEND VTK_MODULES_ENABLED "${_module}")
    endif()
  endforeach()
endif()
unset(smtk_module_dir)

set(smtk_cmake_dir @SMTK_MODULE_DIR@)

set(SMTK_DATA_DIR @smtk_data_dir@)

#When building documentation internally we re-import ourselves, which
#doesnt work and generates CMake warnings. So we will only look for SMTK
#if the smtkCore target doesn't already exist
if(TARGET smtkCore)
else()

  find_package(Boost @SMTK_MINIMUM_BOOST_VERSION@
             COMPONENTS @required_boost_components@ REQUIRED)
  find_package(nlohmann_json REQUIRED)
  find_package(pegtl REQUIRED)
  find_package(MOAB REQUIRED)

  set(SMTK_ENABLE_QT_SUPPORT @SMTK_ENABLE_QT_SUPPORT@)
  if(SMTK_ENABLE_QT_SUPPORT)
    find_package(Qt5 REQUIRED COMPONENTS Core OpenGL Widgets)
  endif()

  set(SMTK_ENABLE_PARAVIEW_SUPPORT @SMTK_ENABLE_PARAVIEW_SUPPORT@)
  if(SMTK_ENABLE_PARAVIEW_SUPPORT)
    find_package(Qt5 REQUIRED COMPONENTS OpenGL)
    find_package(ParaView)
  endif()

  set(SMTK_ENABLE_VTK_SUPPORT @SMTK_ENABLE_VTK_SUPPORT@)
  if(SMTK_ENABLE_VTK_SUPPORT AND NOT SMTK_ENABLE_PARAVIEW_SUPPORT)
    find_package(VTK REQUIRED)
  endif()

  set(SMTK_ENABLE_REMUS_SUPPORT @SMTK_ENABLE_REMUS_SUPPORT@)
  if(SMTK_ENABLE_REMUS_SUPPORT)
    find_package(Remus REQUIRED)
  endif()

  # if SMTK has python bindings enabled and is non-relocatable (i.e. this is
  # SMTK's build directory or SMTK was configured with SMTK_RELOCATABLE_INSTALL
  # set to False), and if there are no preexisting hint variables for finding
  # a python library/include dir/executable, we use the python SMTK found to
  # seed the python hint variables. We do not call find_package() for PythonLibs
  # or PythonInterp, however, since the consuming program should do that (and
  # things get all weird when they are called repeatedly).
  set(SMTK_ENABLE_PYTHON_WRAPPING @SMTK_ENABLE_PYTHON_WRAPPING@)
  if(SMTK_ENABLE_PYTHON_WRAPPING)

    set(SMTK_PYTHON_EXECUTABLE "@smtk_python_executable@")
    set(SMTK_PYTHON_LIBRARY "@smtk_python_library@")
    set(SMTK_PYTHON_INCLUDE_DIR "@smtk_python_include_dir@")
    # SMTK_PYTHON_MDOULEDIR is the location of the smtk python module relative
    # to the location of this file.
    set(SMTK_PYTHON_MODULEDIR "@smtk_python_moduledir@")

    if (SMTK_PYTHON_EXECUTABLE AND NOT PYTHON_EXECUTABLE)
      set(PYTHON_EXECUTABLE ${SMTK_PYTHON_EXECUTABLE})
    endif ()

    if (SMTK_PYTHON_LIBRARY AND NOT PYTHON_LIBRARY)
      set(PYTHON_LIBRARY ${SMTK_PYTHON_LIBRARY})
    endif ()

    if (SMTK_PYTHON_INCLUDE_DIR AND NOT PYTHON_INCLUDE_DIR)
      set(PYTHON_INCLUDE_DIR ${SMTK_PYTHON_INCLUDE_DIR})
    endif ()

    find_package(pybind11 @pybind11_VERSION@)
  endif()

  set(SMTK_PLUGINS @SMTK_PLUGINS@)

  include("@SMTK_CONFIG_DIR@/smtkTargets.cmake")
endif()

#------------------------------------------------------------------------------
# restore CMAKE_PREFIX_PATH
#------------------------------------------------------------------------------

set(CMAKE_PREFIX_PATH "${CMAKE_PREFIX_PATH_save}")
unset(CMAKE_PREFIX_PATH_save)
