## Attribute Resource Changes

* FileSystemItem::ValueAsString() now returns "" when the item is not set.
