## Add icons for applications in dark mode

For users whose system settings use dark mode, a complimentary set of
white icons has been added.

### Developer changes

Icons should be added to SMTK in duplicate with "_b" and "_w" prefixes
to denote their use in light and dark mode, respectively.

### User-facing changes

Users with dark mode enabled will now be able to see their icons.
